####### VARIABLES DEFINITION ########

variable "default-aws-region" {
  default = "eu-west-3"
}

variable "contact" {
  default = "Georges KEMAYO"
}