####### LAMBDA DEFINITION ########

resource "aws_lambda_function" "prod-demo-aws-lambda" {
  filename         = "../demo-aws-lambda/target/demo-aws-lambda-0.0.1-SNAPSHOT.jar"
  function_name    = "prod-demo-aws-lambda"
  role             = aws_iam_role.prod-demo-aws-lambda-role.arn
  handler          = "com.gkemayo.lambda.DemoAwsLambdaHandler"
  source_code_hash = "${filebase64sha256("../demo-aws-lambda/target/demo-aws-lambda-0.0.1-SNAPSHOT.jar")}"
  runtime          = "java8"
  timeout = "30"
  memory_size = "512"

  environment {
    variables = {
       "TZ" = "Europe/Paris"
    }
  }

  dead_letter_config {
    target_arn = aws_sqs_queue.prod-demo-aws-lambda-sqs-dlq.arn
  }
}

resource "aws_lambda_event_source_mapping" "prod-demo-aws-lambda-event-source" {
  event_source_arn  = "${aws_sqs_queue.prod-demo-aws-lambda-sqs.arn}"
  function_name     = "${aws_lambda_function.prod-demo-aws-lambda.arn}"
}



/* resource "aws_lambda_function_event_invoke_config" "prod-demo-aws-lambda-failure-destination" {
  function_name = aws_lambda_alias.prod-demo-aws-lambda

  destination_config {
    on_failure {
      destination = aws_sqs_queue.prod-demo-aws-lambda-sqs-dlq.arn
    }
  }
  maximum_event_age_in_seconds = 60
  maximum_retry_attempts       = 1
}  */
