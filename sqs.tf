####### SQS QUEUE DEFINITION ########

resource "aws_sqs_queue" "prod-demo-aws-lambda-sqs" {
  name                      = "prod-demo-aws-lambda-sqs"
  delay_seconds             = 0
  max_message_size          = 262144
  message_retention_seconds = 345600
  receive_wait_time_seconds = 10
  redrive_policy            = "{\"deadLetterTargetArn\":\"${aws_sqs_queue.prod-demo-aws-lambda-sqs-dlq.arn}\",\"maxReceiveCount\":2}"

  tags = {
    Environment = "production"
    Contact = "${var.contact}"
  }
}

resource "aws_sqs_queue" "prod-demo-aws-lambda-sqs-dlq" {
  name                      = "prod-demo-aws-lambda-sqs-dlq"
  delay_seconds             = 0
  max_message_size          = 262144
  message_retention_seconds = 345600
  receive_wait_time_seconds = 10
  
  tags = {
    Environment = "production"
    Contact = "${var.contact}"
  }
}

####### SQS QUEUE POLICY DEFINITION ########

resource "aws_sqs_queue_policy" "prod-demo-aws-lambda-sqs-policy" {
  queue_url = "${aws_sqs_queue.prod-demo-aws-lambda-sqs.id}"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "sqspolicy1",
  "Statement": [
    {
      "Sid": "First",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "${data.aws_caller_identity.current.arn}",
          "${aws_iam_role.prod-demo-aws-lambda-role.arn}"
        ]
      },
      "Action": "SQS:*",
      "Resource": "${aws_sqs_queue.prod-demo-aws-lambda-sqs.arn}"
    }
  ]
}  
POLICY
}

resource "aws_sqs_queue_policy" "prod-demo-aws-lambda-sqs-dlq-policy" {
  queue_url = "${aws_sqs_queue.prod-demo-aws-lambda-sqs-dlq.id}"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Id": "sqspolicy2",
  "Statement": [
    {
      "Sid": "Second",
      "Effect": "Allow",
      "Principal": {
        "AWS": [
          "${data.aws_caller_identity.current.arn}",  
          "${aws_iam_role.prod-demo-aws-lambda-role.arn}"
        ]
      },
      "Action": "SQS:*",
      "Resource": "${aws_sqs_queue.prod-demo-aws-lambda-sqs-dlq.arn}"
    }
  ]
}
POLICY
}
