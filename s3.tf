####### S3 DEFINITION ########

resource "aws_s3_bucket" "prod-demo-aws-lambda-s3" {
  bucket = "prod-demo-aws-lambda-s3"
  acl    = "private"

  tags = {
    #Name        = "My bucket"
    Environment = "production"
    Contact = "${var.contact}"
  }
}

####### S3 POLICY DEFINITION ########

resource "aws_s3_bucket_policy" "prod-demo-aws-lambda-s3-policy" {
  bucket = "${aws_s3_bucket.prod-demo-aws-lambda-s3.id}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "MYBUCKETPOLICY",
    "Statement": [
        {
            "Sid": "IPAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                    "${data.aws_caller_identity.current.arn}",
                    "${aws_iam_role.prod-demo-aws-lambda-role.arn}"
                ]
            },
            "Action": "s3:*",
            "Resource": "${aws_s3_bucket.prod-demo-aws-lambda-s3.arn}"
        }
    ]
}
POLICY
}