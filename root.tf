####### PROVIDER DEFINITION ########

provider "aws" {
  region = "${var.default-aws-region}"
}