####### LAMBDA ROLE DEFINITION ########

resource "aws_iam_role" "prod-demo-aws-lambda-role" {
  name = "prod-demo-aws-lambda-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

####### LAMBDA POLICIES ATTACHEMENT DEFINITION ########

resource "aws_iam_policy" "dead-letter-queue-policy" {
  name        = "dead-letter-queue-policy"
  description = "dead letter queue policy"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "sqs:SendMessage",
            "Resource": "${aws_sqs_queue.prod-demo-aws-lambda-sqs-dlq.arn}"
        }
    ]
}
EOF
}

resource "aws_iam_policy" "s3-full-access-policy" {
  name        = "s3-full-access-policy"
  description = "s3 full access policy"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "AWSLambdaSQSQueueExecutionRole" {
  role       = "${aws_iam_role.prod-demo-aws-lambda-role.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaSQSQueueExecutionRole"
}

resource "aws_iam_role_policy_attachment" "AWSLambdaBasicExecutionRole" {
  role       = "${aws_iam_role.prod-demo-aws-lambda-role.name}"
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_role_policy_attachment" "s3-full-access-policy" {
  role       = "${aws_iam_role.prod-demo-aws-lambda-role.name}"
  policy_arn = "${aws_iam_policy.s3-full-access-policy.arn}"
}

resource "aws_iam_role_policy_attachment" "dead-letter-queue-policy" {
  role       = "${aws_iam_role.prod-demo-aws-lambda-role.name}"
  policy_arn = "${aws_iam_policy.dead-letter-queue-policy.arn}"
}